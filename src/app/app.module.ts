import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginpageComponent } from './auth/loginpage/loginpage.component';
import { NavbarpageComponent } from './share/navbarpage/navbarpage.component';
import { AlluserpageComponent } from './dashbord/alluserpage/alluserpage.component';
import { DashboardComponent } from './dashbord/dashboard/dashboard.component';
import { ManageCategoryComponent } from './dashbord/manage-category/manage-category.component';
import { HttpClientModule } from '@angular/common/http';
import { AdduserComponent } from './dashbord/adduser/adduser.component';
import { ManageMembershipComponent } from './dashbord/manage-membership/manage-membership.component';
import { AddmembershipComponent } from './dashbord/addmembership/addmembership.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ManageCofounderComponent } from './dashbord/manage-cofounder/manage-cofounder.component';
import { ForgetpageComponent } from './auth/forgetpage/forgetpage.component';
import { SubHeaderComponent } from './share/sub-header/sub-header.component';
import { MetorComponent } from './dashbord/metor/metor.component';
import { AddMentorComponent } from './dashbord/add-mentor/add-mentor.component';
import { PrivacyPolicyComponent } from './dashbord/privacy-policy/privacy-policy.component';
import { TermsConditionComponent } from './dashbord/terms-condition/terms-condition.component';
import { AboutUsComponent } from './dashbord/about-us/about-us.component';
import { NotificationComponent } from './dashbord/notification/notification.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginpageComponent,
    NavbarpageComponent,
    AlluserpageComponent,
    DashboardComponent,
    ManageCategoryComponent,
    AdduserComponent,
    ManageMembershipComponent,
    AddmembershipComponent,
    ManageCofounderComponent,
    ForgetpageComponent,
    SubHeaderComponent,
    MetorComponent,
    AddMentorComponent,
    PrivacyPolicyComponent,
    TermsConditionComponent,
    AboutUsComponent,
    NotificationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
