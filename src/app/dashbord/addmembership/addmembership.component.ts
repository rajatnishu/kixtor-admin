import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiServiceService } from 'src/app/api-service.service';

@Component({
  selector: 'app-addmembership',
  templateUrl: './addmembership.component.html',
  styleUrls: ['./addmembership.component.css']
})
export class AddmembershipComponent implements OnInit {
  isEdit: boolean = false;
  name: any = null;
  amount: any;
  months: any;
  viewPhone: any;
  connectMessage: any;
  id: any;
  isLoading: boolean;
  constructor(private apiservice: ApiServiceService, private router: Router, private toastr: ToastrService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(data => {
      this.id = data["id"];
      if (this.id != -1) {
        this.isLoading = true;
        this.getPackageDetails(this.id);
        this.isEdit = true;
      }
    })
  }

  async getPackageDetails(id: any) {
    let data = {
      id: id
    }
    await this.apiservice.postMethod("/admin/membershipbyid", data).subscribe((data: any) => {
      let details = data.data;
      this.name = details.name;
      this.amount = details.amount;
      this.months = details.months;
      this.viewPhone = details.view_phone;
      this.connectMessage = details.connect_message;
      this.isLoading = false;
    })
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  submit() {
    if (this.name && this.amount && this.months && this.viewPhone && this.connectMessage) {
      let data: any = {
        name: this.name,
        amount: this.amount,
        months: this.months,
        view_phone: this.viewPhone,
        connect_message: this.connectMessage
      }
      if (!this.isEdit) {
        this.isLoading = true;
        this.apiservice.postMethod('/admin/addmembership', data).subscribe((res: any) => {
          if (res.status == 200) {
            this.isLoading = false;
            this.router.navigateByUrl('/dashbord/manage-membership');
            this.toastr.success("", "Membership added successfully");
          } else {
            this.isLoading = false;
            this.toastr.error("", "This package have already added!");
          }
        })
      } else {
        data._id = this.id;
        this.apiservice.postMethod('/admin/updatemembership', data).subscribe((res:any) => {
          if (res.status == 200) {
            this.isLoading = false;
            this.router.navigateByUrl('/dashbord/manage-membership');
            this.toastr.success("", "Membership updated successfully");
          } else {
            this.isLoading = false;
            this.toastr.error("", "This package have already added!");
          }
        }, (err) => {
          this.isLoading = false;
        })
      }
    }
    else {
      this.toastr.error("", "Please fill all mandatory field")
    }

  }
}
