import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCofounderComponent } from './manage-cofounder.component';

describe('ManageCofounderComponent', () => {
  let component: ManageCofounderComponent;
  let fixture: ComponentFixture<ManageCofounderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageCofounderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCofounderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
