import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiServiceService } from 'src/app/api-service.service';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';

declare const $: any;

@Component({
  selector: 'app-manage-cofounder',
  templateUrl: './manage-cofounder.component.html',
  styleUrls: ['./manage-cofounder.component.css']
})
export class ManageCofounderComponent implements OnInit {
  hidefrom = true;
  categoryName: any;
  allCategory: any = [];
  isLoading: boolean;
  hidebutton = true
  _id: any
  constructor(private http: HttpClient, private toastr: ToastrService, private apiservice: ApiServiceService) { }
  ngOnInit() {
    this.getAllcategory();
  }
  datatable() {
    $(function () {
      $("#example1").DataTable();
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
      });
    });
  }

  showfrom() {
    this._id = null;
    this.hidefrom = false;
    this.hidebutton = true
    this.categoryName = ""
  }

  hidedata() {
    this.hidefrom = true
    this.datatable()
  }


  Updatecat() {
    if (this.categoryName) {
      let data = {
        _id: this._id,
        name: this.categoryName
      }
      this.apiservice.postMethod('/admin/updateskill', data).subscribe(data => {
        this.hidefrom = true;
        this.toastr.success("", "Skill updated successfully");
        this.getAllcategory();
      })
    } else {
      this.toastr.error("", "Please enter new skill set")
    }
  }

  addCategory() {
    if (this.categoryName) {
      let data = {
        name: this.categoryName
      }
      this.isLoading = true;
      this.apiservice.postMethod('/admin/addskill', data).subscribe(data => {
        this.hidefrom = true;
        this.toastr.success("", "Skill added successfully");
        this.getAllcategory();
        this.isLoading = false;
      },
        (err) => {
          this.isLoading = false;
        })
    } else {
      this.toastr.error("", "Please enter new skill set")
    }
  }

  getAllcategory() {
    this.isLoading = true;
    this.apiservice.getMethod('/admin/allskill').subscribe((data: any) => {
      this.allCategory = data.data;
      this.isLoading = false;
      this.datatable();
    },
      (err) => {
        this.isLoading = false;
      })
  }


  editcategory(data) {
    this._id = data._id
    this.categoryName = data.name
    this.hidefrom = false;
    this.hidebutton = false
  }

  deleteCategory(id, index) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want delete this skill!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        let data = {
          id: id
        }
        this.apiservice.postMethod('/admin/deleteskills', data).subscribe((data: any) => {
          this.allCategory.splice(index, 1)
          Swal.fire(
            'Deleted!',
            'Your skill has been deleted.',
            'success'
          )
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your skill is safe :)',
          'error'
        )
      }
    })
  }
}
