import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/api-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { countryList} from './../../countryList'

@Component({
  selector: 'app-add-mentor',
  templateUrl: './add-mentor.component.html',
  styleUrls: ['./add-mentor.component.css']
})
export class AddMentorComponent implements OnInit {
  email: any;
  name: any;
  post: any;
  cost: any;
  id: any;
  isEdit: boolean = false;
  countryList = countryList;
  country:any=null;
  constructor(private apiservice: ApiServiceService, private toastr: ToastrService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(data => {
      if (parseInt(data['id']) > -1) {
        this.id = data['id'];
        this.isEdit = true
        this.getMentorDetails()
      }
    })
  }

  async getMentorDetails() {
    let data = {
      id: this.id
    }
    await this.apiservice.postMethod("/admin/mentorbyid", data).subscribe((res: any) => {
      debugger
      this.email = res.data.mentor_email;
      this.name = res.data.mentor_name;
      this.post = res.data.mentor_post;
      this.cost = res.data.mentor_cost;
      this.country = res.data.country;
    })
  }

  mentor_image :any
  image:boolean =true
  addprofile :any

  choosePhoto(element) {
    const self = this;
    var uploadImg = element.target.files[0]
    var FileSize = element.target.files[0].size / 1024 / 1024;
    if (FileSize > 2) {
      return this.toastr.error("File size exceeds 2 MB");
    }
    var fileName = element.target.files[0].name;
      self.mentor_image = fileName
      const reader = new FileReader();
      reader.readAsDataURL(uploadImg);
      reader.onload = () => {
        console.log(reader.result);
        this.image = false
        this.addprofile = reader.result;
        // this.editImage =false;
      };
  }


  async addmentor() {
    if (this.email && this.name && this.post && this.cost && this.country) {

      let data: any = {
        mentor_email: this.email,
        mentor_name: this.name,
        mentor_post: this.post,
        mentor_cost: this.cost,
        mentor_image: this.mentor_image,
        profile : this.addprofile,
        country:this.country
      }
      debugger
      if (!this.isEdit) {
        await this.apiservice.postMethod("/addmentor", data).subscribe((data: any) => {
          if (data.status == 200) {
            this.router.navigateByUrl('/dashbord/mentor')
            this.toastr.success("", "Mentor added successfully")
          }
        }, (err) => {
          console.log(err)
        })
      } else {
        data._id = this.id;
        await this.apiservice.postMethod("/admin/updatementor", data).subscribe((data: any) => {
          data
          this.router.navigateByUrl('/dashbord/mentor')
          this.toastr.success("", "Mentor updated successfully")
        }, (err) => {
          console.log(err)
        })
      }
    } else {
      this.toastr.error(" ", "Pleas fill all required field")
    }

  }

  gotomenterpage() {
    this.router.navigate(['/dashbord/mentor'])
  }

}
