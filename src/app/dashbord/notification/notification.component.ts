import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/api-service.service';
import * as moment from 'moment/moment';
@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  allNotification: any;
  constructor(private apiService: ApiServiceService) { }

  ngOnInit(): void {
    this.getAllNotification();
  }

  getAllNotification() {
    debugger
    this.apiService.getMethod('/admin/allnotification').subscribe((res: any) => {
      this.allNotification  = res.data;
    })
  }

  getTime(value){
    return moment(value).calendar(); 
  }
}
