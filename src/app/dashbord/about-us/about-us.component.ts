import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ApiServiceService } from 'src/app/api-service.service';
declare var CKEDITOR:any;

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {
  isEdit:boolean=false;
  id:any;
  about:any;
  isLoading:boolean=false;
  constructor(private apiservice: ApiServiceService, private toastr: ToastrService) { }

  ngOnInit(): void {
    CKEDITOR.replace( 'editor1' );
    this.AboutUs();
  }
  AboutUs() {
    let data = {
      type: 'About'
    }
    this.isLoading =true;
    this.apiservice.postMethod('/admin/allcms', data).subscribe((res: any) => {
      if(res && res.data){
        this.isEdit =true;
      }
      this.isLoading =false;
      if (res.data && res.data.content) {
        this.about = res.data.content;
        CKEDITOR.instances.editor1.setData(this.about)
        this.id = res.data._id;
      }
    }, (err) => {
      this.isLoading =false;
      console.log(err)
    })
  }

  submit() {
    this.about =  CKEDITOR.instances.editor1.getData()
    if (this.about) {
      let data:any={
        content:this.about,
        type:'About'
      }
      if (!this.isEdit) {
        this.isLoading =true;
        this.apiservice.postMethod('/admin/addcms',data).subscribe((res:any)=>{
          this.isLoading =false;
          this.toastr.success('', "About us added successfully")
        },(err)=>{
          this.isLoading =false;
          console.log(err)
        })
      } else {
         data._id= this.id
         this.isLoading =true;
        this.apiservice.postMethod('/admin/updatecms',data).subscribe((res:any)=>{
          this.isLoading =false;
          this.toastr.success('', "About us updated successfully")
        },(err)=>{
          this.isLoading =false;
          console.log(err)
        })
      }
    } else {
      this.toastr.error('', "Please enter about us")
    }
  }
}
