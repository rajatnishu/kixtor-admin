import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MetorComponent } from './metor.component';

describe('MetorComponent', () => {
  let component: MetorComponent;
  let fixture: ComponentFixture<MetorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MetorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MetorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
