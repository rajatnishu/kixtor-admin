import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, } from "@angular/router";
import { ApiServiceService } from 'src/app/api-service.service';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
declare const $: any;

@Component({
  selector: 'app-metor',
  templateUrl: './metor.component.html',
  styleUrls: ['./metor.component.css']
})
export class MetorComponent implements OnInit {

  constructor(private router: Router, private apiservice: ApiServiceService, private toastr: ToastrService) { }
  step = 'true2'
  getmentor: any = []

  ngOnInit(): void {
    this.getallmentor()
  }


  highlight(data) {
    this.step = data
  }

  getallmentor() {
    this.apiservice.getMethod('/admin/allmentor').subscribe((data: any) => {
      if (data.status == 200) {
        this.getmentor = data.result
        this.datatable()
      }
    })
  }

  datatable() {
    $(function () {
      $("#example1").DataTable();
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
      });
    });
  }

  delete(value, index) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want delete this mentor!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your mentor data has been deleted.',
          'success'
        )
        let data = {
          _id: value._id
        }
        this.apiservice.postMethod('/admin/deletementor', data).subscribe((data: any) => {
          if (data.status == 200) {
            this.datatable()
            this.getmentor.splice(index, 1)
          }
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your data is safe :)',
          'error'
        )
      }
    })
  
  }

  gotopage() {
    this.router.navigateByUrl('/dashbord/add-mentor/-1')
  }

  editMentor(id){
    this.router.navigateByUrl('/dashbord/add-mentor/'+id)
  }
}
