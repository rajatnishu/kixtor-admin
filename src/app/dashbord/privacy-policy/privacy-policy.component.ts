import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ApiServiceService } from 'src/app/api-service.service';
declare var CKEDITOR:any;

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.css']
})
export class PrivacyPolicyComponent implements OnInit {
  isEdit: boolean = false;
  privacy: any;
  id: any;
  isLoading:boolean=false;

  constructor(private apiservice: ApiServiceService, private toastr: ToastrService) { }

  ngOnInit(): void {
    CKEDITOR.replace( 'editor2' );
    this.getPrivacyPolicy()
  }
  getPrivacyPolicy() {
    let data = {
      type: 'Privacy'
    }
    this.isLoading =true;
    this.apiservice.postMethod('/admin/allcms', data).subscribe((res: any) => {
      if(res && res.data){
        this.isEdit =true;
      }
      this.isLoading =false;
      if (res.data && res.data.content) {
        this.privacy = res.data.content;
        CKEDITOR.instances.editor2.setData(this.privacy);
        this.id = res.data._id;
      }
    }, (err) => {
      this.isLoading =false;
      console.log(err)
    })
  }

  submit() {
    this.privacy =  CKEDITOR.instances.editor2.getData()
    if (this.privacy) {
      let data:any={
        content:this.privacy,
        type:'Privacy'
      }
      if (!this.isEdit) {
        this.isLoading =true;
        this.apiservice.postMethod('/admin/addcms',data).subscribe((res:any)=>{
          this.isLoading =false;
          this.toastr.success('', "Privacy Policy added successfully")
        },(err)=>{
          this.isLoading =false;
          console.log(err)
        })
      } else {
         data._id= this.id
         this.isLoading = true;
        this.apiservice.postMethod('/admin/updatecms',data).subscribe((res:any)=>{
          this.isLoading =false;
          this.toastr.success('', "Privacy Policy updated successfully")
        },(err)=>{
          this.isLoading =false;
          console.log(err)
        })
      }
    } else {
      this.toastr.error('', "Please enter privacy policy")
    }
  }
}
