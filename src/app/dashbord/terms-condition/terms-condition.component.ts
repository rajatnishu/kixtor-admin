import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ApiServiceService } from 'src/app/api-service.service';
declare var CKEDITOR:any;
@Component({
  selector: 'app-terms-condition',
  templateUrl: './terms-condition.component.html',
  styleUrls: ['./terms-condition.component.css']
})
export class TermsConditionComponent implements OnInit {
  isEdit: boolean = false;
  terms: any;
  id: any;
  isLoading:boolean=false;

  constructor(private apiservice: ApiServiceService, private toastr: ToastrService) { }

  ngOnInit(): void {
    CKEDITOR.replace( 'editor3' );
    this.getTemscondition();
  }
  getTemscondition() {
    debugger
    let data = {
      type: 'Terms'
    }
    this.isLoading =true;
    this.apiservice.postMethod('/admin/allcms', data).subscribe((res: any) => {
      if(res && res.data){
        this.isEdit =true;
      }
      this.isLoading =false;
      if (res.data && res.data.content) {
        this.terms = res.data.content;
        CKEDITOR.instances.editor3.setData(this.terms);
        this.id = res.data._id;
      }
    }, (err) => {
      this.isLoading =false;
      console.log(err)
    })
  }

  submit() {
    this.terms =  CKEDITOR.instances.editor3.getData()
    if (this.terms) {
      let data:any={
       
        content: this.terms,
        type:'Terms'
      }
      if (!this.isEdit) {
        this.isLoading =true;
        this.apiservice.postMethod('/admin/addcms',data).subscribe((res:any)=>{
          this.isLoading =false;
          this.toastr.success('', "Terms and Condition added successfully")
        },(err)=>{
          this.isLoading =false;
          console.log(err)
        })
      } else {
         data._id= this.id
         this.isLoading =true;
        this.apiservice.postMethod('/admin/updatecms',data).subscribe((res:any)=>{
          this.isLoading =false;
          this.toastr.success('', "Terms and Condition updated successfully")
        },(err)=>{
          this.isLoading =false;
          console.log(err)
        })
      }
    } else {
      this.toastr.error('', "Please enter terms and condition")
    }
  }
}
