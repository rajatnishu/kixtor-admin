import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlluserpageComponent } from './alluserpage.component';

describe('AlluserpageComponent', () => {
  let component: AlluserpageComponent;
  let fixture: ComponentFixture<AlluserpageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlluserpageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlluserpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
