import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiServiceService } from 'src/app/api-service.service';
import Swal from 'sweetalert2';
declare const $: any;
@Component({
  selector: 'app-alluserpage',
  templateUrl: './alluserpage.component.html',
  styleUrls: ['./alluserpage.component.css']
})
export class AlluserpageComponent implements OnInit {
  allUser: any;
  isLoading:boolean =true;
  details:any={}
  constructor(private router: Router, private apiservice: ApiServiceService) { }

  ngOnInit(): void {
    this.getAllUser() 
  }
  hidefrom = true
  datatable() {
    $(function () {
      $("#example1").DataTable();
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
      });
    });
  }

  getAllUser() {
    this.isLoading =true;
    this.apiservice.getMethod('/admin/alluser').subscribe((data: any) => {
      let userData = data.data
      this.allUser = userData.filter((da)=> da.role != 'ADMIN');
      this.isLoading =false;
      this.datatable()
    },
    (err)=>{
      this.isLoading =false;
    })
  }

  addUser() {
    this.router.navigateByUrl('/dashbord/add-user/-1')
  }

  editUser(id){
    this.router.navigateByUrl('/dashbord/add-user/'+id)
  }

  deleteUser(id, index) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want delete this user!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your user data has been deleted.',
          'success'
        )
        let data = {
          id: id
        }
        this.apiservice.postMethod('/admin/deleteuser', data).subscribe((data: any) => {
          this.allUser.splice(index, 1)
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your data is safe :)',
          'error'
        )
      }
    }) 
  }

  accountapprove(data, text){
      let obj ={
    _id : data._id,
     is_verify : text
      }
      this.apiservice.postMethod('/admin/approveaccount', obj).subscribe((data: any) => {
        if(data.status== 200){
             this.getAllUser()
        }
      })
  }

  openDetail(data){
    let arr=[];
    for(let i=0;i<data.stage.length;i++){
      if(data.stage[i].value){
        arr.push(data.stage[i].name);
      }
    }
    data.stageValue =  arr.toString();
    data.profile = this.apiservice.apiUrl +'/static/'+ data.Uploaded_filename
    this.details= data
  }


}
