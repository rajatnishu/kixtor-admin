import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiServiceService } from 'src/app/api-service.service';
import Swal from 'sweetalert2';
declare const $: any;

@Component({
  selector: 'app-manage-membership',
  templateUrl: './manage-membership.component.html',
  styleUrls: ['./manage-membership.component.css']
})
export class ManageMembershipComponent implements OnInit {
  isLoading: boolean;
  allMembership: any;

  constructor(private router:Router,private apiservice :ApiServiceService) { }

  ngOnInit(): void {
    this.getAllMembership();
  }

  addMembership(){
    this.router.navigateByUrl('/dashbord/addmembership/-1');
  }

  getAllMembership(){
    this.isLoading=true;
    this.apiservice.getMethod("/admin/allmembership").subscribe((data:any)=>{
      this.allMembership = data.data;
      this.isLoading=false;
      this.datatable();
    },
    (err)=>{
      this.isLoading=false;
    })
  }

  datatable() {
    $(function () {
      $("#example1").DataTable();
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
      });
    });
  }


  deleteMembership(id, index) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want delete this membership!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        let data = {
          id: id
        }
        this.apiservice.postMethod('/admin/deletemembership', data).subscribe((data: any) => {
          this.allMembership.splice(index, 1)
          Swal.fire(
            'Deleted!',
            'Your membership has been deleted.',
            'success'
          )
        })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your membership is safe :)',
          'error'
        )
      }
    })
  }


  editMembership(id){
    this.router.navigateByUrl('/dashbord/addmembership/' + id);
  }
}
