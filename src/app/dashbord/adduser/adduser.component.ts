import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiServiceService } from 'src/app/api-service.service';
import { countryList } from '../../countryList';
import { ToastrService } from 'ngx-toastr';
import {CodeList} from './../../countyCode'
@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {

  step: any = "1";
  name: any;
  email: any;
  contactNumber: any;
  gender: any;
  password: any;
  rePassword: any;
  country: any = null;
  countryList:any= countryList;
  state: any=null;
  strengths: any =null;
  profilePic: any;
  skills: any =null;
  stage: any;
  stateList :any=[];
  stepFirstMandatory = ["name", "contactNumber", "email", "gender", "state", "country"]
  setSecondMandatory = ["strengths", "skills", "stage"];
  errorMessage: any;
  isError: boolean = false;
  isEdit:boolean=false;
  id:any;
  countryCode:any =null;
  codeList = CodeList;
  editImage:boolean =false;
  stageList = [
    { name: "Ideation", value: false },
    { name: "I have a Business Plan", value: false },
    { name: "Pilot Testing OR MVP", value: false },
    { name: "I want to scale up", value: false }
  ];
  industryList=[
       {name: "3D Printing",value:false},
       {name: "Artificial Intelligence",value:false},
       {name: "Machine Learning",value:false},
       {name: "Legal",value:false},
       {name: "Design",value:false},
       {name: "Substainability",value:false},
       {name: "Supply Chain",value:false},
       {name: "Logistics",value:false},
       {name: "Green Initiatives",value:false},
       {name: "MLM / Network Marketing",value:false},
       {name: "Consumer Products",value:false},
       {name: "Finance",value:false},
       {name: "Social",value:false},
       {name: "Real Estate",value:false},
       {name: "Biotech",value:false},
       {name: "Events",value:false},
       {name: "Fitness",value:false},
       {name: "Tourism",value:false},
       {name: "Hospitality",value:false},
       {name: "Technology",value:false},
       {name: "Consulting",value:false},
       {name: "Ecommerce",value:false},
       {name: "Fashion",value:false},
       {name: "Healthcare",value:false},
       {name: "Recruitments",value:false},
       {name: "Critical Infrastucture",value:false},
       {name: "Security",value:false},
       {name: "Agriculture",value:false},
       {name: "Education",value:false},
       {name: "Others",value:false}
  ]
  allStrengths: any;
  allSkills: any;
  isLoading:boolean;
  addprofile :any;
  Uploaded_filename :any;
  image = true;
  constructor(private apiservice: ApiServiceService, private router: Router, private toastr: ToastrService, private route: ActivatedRoute) { }

  async ngOnInit() {
   // await this.getCountryList();
    this.getAllStrengths();
    this.getAllSkills();
    this.route.params.subscribe(data => {
      this.id = data["id"];
      if (this.id != -1) {
        this.isLoading =true;
        this.getUserDetails(this.id);
        this.isEdit = true;
      }
    })
  }
  // async getCountryList(){
  //   await this.apiservice.getCountry().subscribe(async(res)=>{
  //     this.countryList = await res
  //   },(err)=>{

  //   })
  // }

  async selectCountry(value){
    this.state= null;
    let data={
      code:value.code
    }
    await this.apiservice.postMethod('/getstate' ,data).subscribe((res)=>{
      this.stateList = res;
    })
  }
  async getUserDetails(id) {
    let data = {
      id: id
    }
    await this.apiservice.postMethod("/admin/userbyid", data).subscribe(async (data: any) => {
      let details = data.data;
      this.country = await this.countryList.find(c=> c.name == details.country);
      this.name = details.name;
      this.contactNumber = details.contact_number;
      this.email = details.email;
      this.password = details.password;
      this.rePassword = details.password;
      this.gender = details.gender;
    
      this.strengths = details.strengths;
      this.profilePic = details.profile_photo;
      this.skills = details.skills;
      this.stageList = details.stage;
      this.countryCode = details.country_code;
      this.Uploaded_filename = details.Uploaded_filename;
      this.editImage =true;
     
      for(let i=0;i<details.industry.length;i++){
        for(let j=0; j < this.industryList.length;j++){
          if(details.industry[i] == this.industryList[j].name){
            this.industryList[j].value = true;
          }
        }
      }
      await this.selectCountry(this.country);
      this.state = details.state;
      this.isLoading=false;
    })
  }
  getAllStrengths(){
    this.apiservice.getMethod('/admin/allcategory').subscribe((data: any) => {
      this.allStrengths = data.data
    })
  }
  getAllSkills(){
    this.apiservice.getMethod('/admin/allskill').subscribe((data: any) => {
      this.allSkills = data.data
    })
  }

  continue(value) {
    let checkMandatory = false;
    if(value== '2'){
      for(let i=0;i<this.stepFirstMandatory.length;i++){
        if(!this[this.stepFirstMandatory[i]]){
          checkMandatory= true;
        }
      }
      if(checkMandatory){
        return this.toastr.error("","Please fill all mandatory field ");
      } else {
        if(this.contactNumber && this.contactNumber.length < 10){
          return this.toastr.error("","Contact number should be 10 digit");
        }
        if(this.email){
          const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          let checkMail = re.test(this.email);
          if(!checkMail){
            return this.toastr.error("","Please enter valid email");
          }
        }
        
        if(this.password){
           if(!this.rePassword){
              return this.toastr.error("","Please enter retype password");              
           }
           if(this.password !== this.rePassword){
              return this.toastr.error("","Password and Retype Password not match");
           }
        }
        this.step = value;
      }
    } else if(value== '3'){
      if(!this.Uploaded_filename){
        return this.toastr.error("","Please select image");
      }
       if(!this.strengths){
        return this.toastr.error("","Please Select Strengths");
       }
       let findStage = this.stageList.find((d)=> d.value == true)
       if(!findStage){
        return this.toastr.error("","Please Select Stage");
       }
       if(!this.skills){
        return this.toastr.error("","Please Select Skill");
       }
       this.step = value;
      }
    // this.step = value;
  }

  back(value) {
    this.step = value;
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  
  choosePhoto(element) {
    const self = this;
    var uploadImg = element.target.files[0]
    var FileSize = element.target.files[0].size / 1024 / 1024;
    if (FileSize > 2) {
      return this.toastr.error("File size exceeds 2 MB");
    }
    var fileName = element.target.files[0].name;
      self.Uploaded_filename = fileName
      const reader = new FileReader();
      reader.readAsDataURL(uploadImg);
      reader.onload = () => {
        console.log(reader.result);
        this.image = false
        this.addprofile = reader.result;
        this.editImage =false;
      };
  }

  selectIndustry(event,i){
    this.industryList[i].value = event.target.checked; 
  }

  selectStrength(event, i) {
    this.stageList[i].value = event.target.checked;
  }

  submit() {
    let industry = [];
    for(let i=0; i< this.industryList.length;i++){
       if(this.industryList[i].value){
          industry.push(this.industryList[i].name);
       }
    }
    if(industry && industry.length == 0){
      return   this.toastr.error('', 'Please Select Industry'); 
    }
    let data:any = {
      name: this.name,
      contact_number: this.contactNumber,
      email: this.email,
      password: this.password,
      country: this.country.name,
      state: this.state,
      gender: this.gender,
      addprofile :this.addprofile || '',
      profile_photo:this.Uploaded_filename,
      Uploaded_filename: this.Uploaded_filename,
      strengths: this.strengths,
      stage: this.stageList,
      skills: this.skills,
      industry:industry,
      is_verify: "approve",
      country_code:this.countryCode
    }
    if(!this.isEdit){
    this.isLoading =true;
    this.apiservice.postMethod('/adduser', data).subscribe((data:any) => {
      if(data.status == 200){
        this.toastr.success('', 'User added successfully');
        this.router.navigateByUrl('/dashbord/manage-user');
        this.isLoading =false;
      } else{
        this.toastr.error('', 'this email already exist');
        this.isLoading =false;
      }
    })
  }
  else{
    data._id = this.id;
    this.isLoading = true;
    this.apiservice.postMethod('/updateuser', data).subscribe(data => {
      data
      this.toastr.success('', 'User updated successfully');
      this.router.navigateByUrl('/dashbord/manage-user');
      this.isLoading =false;
    }, (err) => {
      this.isLoading =false;
      console.log(err)
    })
  }
  }
}
