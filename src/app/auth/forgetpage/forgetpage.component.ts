import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, } from "@angular/router";
import { ApiServiceService } from 'src/app/api-service.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-forgetpage',
  templateUrl: './forgetpage.component.html',
  styleUrls: ['./forgetpage.component.css']
})
export class ForgetpageComponent implements OnInit {

  constructor(private router: Router, private apiService: ApiServiceService, private toastr: ToastrService) { }

  email: any
  isLoading = false
  shownewinputbox = true
  ngOnInit(): void {
  }


  gotopage() {
    this.router.navigate(['/auth/loginpage'])
  }

  requsetnewpage() {
    debugger
    if (this.email) {
      this.isLoading =  true
      let data = {
        email: this.email
      }
      this.apiService.postMethod('/admin/requestemail', data).subscribe((response: any) => {
        if (response.status == 200) {
          this.isLoading =  false
          this.shownewinputbox = false
          this.toastr.success("", "Your otp send on your mail you can create your new password")
        } else {
          this.isLoading =  false
          this.toastr.error("", "Invalid email")
        }
      })
    } else {
      this.toastr.error("", "enter email name")
    }
  }
  otp:any
  newpassword :any
  conformpassword :any
  getconformpassword(){
    if(this.otp && this.newpassword && this.conformpassword){
      if(this.newpassword ==  this.conformpassword){
        let data={
         otp: this.otp,
         email: this.email,
        newpassword : this.newpassword 
        }
        this.apiService.postMethod('/admin/updatepassword', data).subscribe((response: any) => {
          if (response.status == 200) {
             this.toastr.success('',"your password update succesfully")
             this.router.navigate(['/dashbord/manage-user'])
          } else{
            this.toastr.error('',"please enter currect otp")
          }
        })
      } else{
        this.toastr.error("", "both password should be match")
      }
    } else{
      this.toastr.error("", "all feild required")
    }
  }
}
