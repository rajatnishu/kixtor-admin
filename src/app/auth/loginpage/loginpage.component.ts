import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, } from "@angular/router";
import { ApiServiceService } from 'src/app/api-service.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  styleUrls: ['./loginpage.component.css']
})
export class LoginpageComponent implements OnInit {
  email: any;
  password: any;
  isLoading = false
  constructor( private router: Router,private apiService: ApiServiceService,private toastr:ToastrService) { }

  ngOnInit(): void {
  }

  login(){
    if(this.email && this.password){
      this.isLoading = true
      let data={
        email:this.email,
        password:this.password
      }
     this.apiService.postMethod('/admin/login',data).subscribe((response:any)=>{
       if(response.status == 200){
        this.isLoading = false
        this.toastr.success("","Login Successfully")
        this.router.navigate(['/dashbord/manage-user'])
       } else {
        this.isLoading = false
        this.toastr.error("","Invalid email and password")
       }
     })
    } else {
      this.isLoading = false
      this.toastr.error("","Please enter email and password")
    }
  }

  gotopage(){
    this.router.navigate(['/auth/forgetpage'])
  }

}
