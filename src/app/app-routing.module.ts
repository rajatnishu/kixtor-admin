import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginpageComponent } from './auth/loginpage/loginpage.component';
import { AlluserpageComponent } from './dashbord/alluserpage/alluserpage.component';
import { DashboardComponent } from './dashbord/dashboard/dashboard.component';
import { ManageCategoryComponent } from './dashbord/manage-category/manage-category.component';
import { AdduserComponent } from './dashbord/adduser/adduser.component';
import { ManageMembershipComponent } from './dashbord/manage-membership/manage-membership.component';
import { AddmembershipComponent } from './dashbord/addmembership/addmembership.component';
import { ManageCofounderComponent } from './dashbord/manage-cofounder/manage-cofounder.component';
import { ForgetpageComponent } from './auth/forgetpage/forgetpage.component';
import { MetorComponent } from './dashbord/metor/metor.component';
import { AddMentorComponent } from './dashbord/add-mentor/add-mentor.component';
import { PrivacyPolicyComponent } from './dashbord/privacy-policy/privacy-policy.component';
import { TermsConditionComponent } from './dashbord/terms-condition/terms-condition.component';
import { AboutUsComponent } from './dashbord/about-us/about-us.component';
import { NotificationComponent } from './dashbord/notification/notification.component';
const routes: Routes = [

  { path: '', redirectTo: 'auth/loginpage', pathMatch: 'full' },
  { path: 'auth/loginpage', component: LoginpageComponent },
  { path: 'auth/forgetpage', component: ForgetpageComponent },
  { path: 'dashbord/manage-user', component: AlluserpageComponent },
  { path: 'dashbord/dashboard', component: DashboardComponent },
  { path: 'dashbord/manage-category', component: ManageCategoryComponent },
  { path: 'dashbord/add-user/:id', component: AdduserComponent },
  { path: 'dashbord/manage-membership', component: ManageMembershipComponent },
  { path: 'dashbord/addmembership/:id', component: AddmembershipComponent },
  { path: 'dashbord/manage-cofounder', component: ManageCofounderComponent },
  { path: 'dashbord/mentor', component: MetorComponent },
  { path: 'dashbord/add-mentor/:id', component: AddMentorComponent },
  { path: 'dashbord/privacy', component: PrivacyPolicyComponent },
  { path: 'dashbord/terms-condition' ,component:TermsConditionComponent},
  { path: 'dashbord/about-us', component:AboutUsComponent },
  { path: 'dashbord/notification', component:NotificationComponent },
  {
    path: 'pages',
    loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
