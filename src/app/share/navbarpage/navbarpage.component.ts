import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { ApiServiceService } from 'src/app/api-service.service';
import * as moment from 'moment/moment';
@Component({
  selector: 'app-navbarpage',
  templateUrl: './navbarpage.component.html',
  styleUrls: ['./navbarpage.component.css']
})
export class NavbarpageComponent implements OnInit {

  constructor( private router: Router,private toast:ToastrService,private apiService: ApiServiceService,) { }
  getnotification : any = []
  ngOnInit(): void {
    this.apiService.getMethod('/admin/allnotification').subscribe((res: any)=>{
      let notification = res.data;
      notification = notification.filter((noti)=> noti.read == false);
      this.getnotification = notification;
    })
  }

  gotopage(value){
    this.router.navigate(['/' + value])
    document.getElementById("sidebar").classList.remove('sidebar-open')
    document.getElementById("sidebar").classList.remove('sidebar-collapse')
  }

  getTime(time){
   return moment(time).fromNow();
  }
  logout(){
    this.toast.success("Logout Successfully")
    this.router.navigateByUrl("/auth/loginpage")
  }
}
