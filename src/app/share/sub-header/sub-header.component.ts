import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.css']
})
export class SubHeaderComponent implements OnInit {
  activeUrl: any;
  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
   this.activeUrl = this.route.snapshot.url[1].path
  
  }

  highlight(data) {
    this.activeUrl = data;
    if (this.activeUrl == 'mentor') {
      this.router.navigateByUrl('/dashbord/mentor');
    } else if (this.activeUrl == 'privacy') {
      this.router.navigateByUrl('/dashbord/privacy');
    } else if (this.activeUrl == 'about-us') {
      this.router.navigateByUrl('/dashbord/about-us');
    } else if (this.activeUrl == 'terms-condition') {
      this.router.navigateByUrl('/dashbord/terms-condition');
    }

  }
}
